#!/bin/bash

# save list of dotfiles as function
oldDir=$HOME/.oldDotfiles
newDir=$HOME/.dotfiles
declare -a paths=( "$(find $newDir/ -type f -name ".[:a-z:]*" | tr '\n' ' ')" )
declare -a files=( "$(find $newDir/ -type f -name ".[:a-z:]*" -exec basename -a {} \; | tr '\n' ' ')" )

if [ ! -d "$oldDir" ]; then
	mkdir -p "$oldDir"
fi


echo "These are the files:"
printf "%s\n" "${files[@]}"
echo ""

# Move original dotfiles to archive
for o in ${files[@]}; do
	if [ -f "$HOME/$o" ]; then
		mv "$HOME"/"$o" "$oldDir"/
#		echo "mv $HOME/$o $oldDir/"
	fi
done

# Make symlink to new files
for i in ${paths[*]}; do
	ln -s "$i" ~/
#	echo "ln -s $i $HOME/"
#	echo "$i"
done
