#!/bin/bash

# .bashrc file which should auto switch between Mac and Linux systems

# OS Test

oscheck() {
    case "$OSTYPE" in
        darwin*)
        export ostype="mac"
        ;;
    esac

	#if [ "$(echo $OSTYPE)" =~ darwin ]; then
	#	export ostype="mac"
	if [ "$OSTYPE" == linux-gnu ]; then
		export ostype="linux"
	else
		return 1
	fi
}

# Figure out which OS we're on

oscheck

#export PS1='\[\e[1;33m\]\u@\h:\W${text}$\[\e[m\] '

# Mac or Linux Aliases

if [ "$ostype" == mac ]; then
	alias truecrypt='/Applications/TrueCrypt.app/Contents/MacOS/Truecrypt --text'
	alias ll='ls -laGh'

elif [ "$ostype" == linux ]; then
	alias ll='ls -lah --color=auto'
fi

# Universal Aliases

# IP Function
myip() {
    printf '%s\n' "WAN IP: $(curl -sL https://calabrocloud.com/ip)"
    if [ $ostype == mac ]; then
        echo "LAN IP: $(ifconfig | grep 'inet ' | grep -v 127.0.0.1 | cut -d: -f2 | awk '{print $2}')"
    elif [ $ostype == linux ]; then
        echo "LAN IP: $(ifconfig | grep 'inet ' | grep -v 127.0.0.1 | cut -d: -f2 | awk '{print $2}')"
    fi
                        # myip:         Public facing IP Address
}

## VIM!
export EDITOR=vim


## Options from Tom Lawrence gibhub.com/flipsidecreations/dotfiles

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

#if [ "$color_prompt" = yes ]; then
#    PS1="\[\033[0;31m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]root\[\033[01;33m\]@\[\033[01;96m\]\h '; else echo '\[\033[0;39m\]\u\[\033[01;33m\]@\[\033[01;96m\]\h '; fi)\[\033[0;31m\]]\342\224\200[\[\033[0;33m\]\w\[\033[0;31m\]]\n\[\033[0;31m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\\$\[\e[0m\] "
#else
#    PS1='┌──[\u@\h]─[\w]\n└──╼ \$ '
#fi
if [ "$color_prompt" = yes ]; then
    if [ $UID == 0 ]; then
        export PS1="\[\e[31m\]\u\[\e[m\]\[\e[32m\]@\[\e[m\]\[\e[32m\]\h\[\e[m\]:[\[\e[36m\]\W\[\e[m\]] \[\e[36m\]\\$\[\e[m\] "
    else
        export PS1="\[\e[92m\]\u\[\e[m\]\[\e[92m\]@\[\e[m\]\[\e[92m\]\h\[\e[m\]:[\[\e[36m\]\W\[\e[m\]] \[\e[36m\]\\$\[\e[m\] "
    fi
fi

# Set 'man' colors
if [ "$color_prompt" = yes ]; then
	man() {
	env \
	LESS_TERMCAP_mb=$'\e[01;31m' \
	LESS_TERMCAP_md=$'\e[01;31m' \
	LESS_TERMCAP_me=$'\e[0m' \
	LESS_TERMCAP_se=$'\e[0m' \
	LESS_TERMCAP_so=$'\e[01;44;33m' \
	LESS_TERMCAP_ue=$'\e[0m' \
	LESS_TERMCAP_us=$'\e[01;33m' \
	man "$@"
	}
fi

unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#
#	export PS1="\[\e[33m\]\u\[\e[m\]\[\e[33m\]@\[\e[m\]\[\e[32m\]\h\[\e[m\]:[\[\e[36m\]\W\[\e[m\]] \[\e[36m\]\\$\[\e[m\] "
#    ;;
#*)
#    ;;
#esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'


# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
